const express = require('express');
const app = express();
const cors = require('cors');
const port = 3000;
const companies = require('./companies.json');

app.use(cors());

app.get('/companies', (req, res) => res.json(companies))

app.listen(port, () => console.log('SmartPay Warm-up Server listening on port ' +port +'!'))